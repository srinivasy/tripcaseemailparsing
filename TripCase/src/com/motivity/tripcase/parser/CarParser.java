package com.motivity.tripcase.parser;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.motivity.tripcase.pojo.Car;
import com.motivity.tripcase.pojo.EMail;
import com.motivity.tripcase.pojo.Parser;
import com.motivity.tripcase.utils.Config;
import com.motivity.tripcase.utils.HTMLBuilder;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.tokensregex.CoreMapExpressionExtractor;
import edu.stanford.nlp.ling.tokensregex.MatchedExpression;
import edu.stanford.nlp.ling.tokensregex.TokenSequencePattern;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.PTBTokenizerAnnotator;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.time.TimeAnnotations.TimexAnnotation;
import edu.stanford.nlp.time.Timex;
import edu.stanford.nlp.util.CoreMap;

public class CarParser {

	public static String emailNameWithoutExt = "";
	private StanfordCoreNLP pipeline;

	public CarParser()
	{
		pipeline = getNLPPipeline();
	}
	public void parse(Parser parser) {
		try {
			emailNameWithoutExt = parser.getEmailFile().replace(".eml", "");
			//System.out.println("STARTED parsing : " + emailNameWithoutExt);
			//System.//out.println(" EMAIL NAME IS : " + emailNameWithoutExt);
			processViaNLP(parser);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void processViaNLP(Parser parser) {

		try {

			if (pipeline != null) {

				Annotation annotation = getAnnotation(parser.getEmail());
				if (annotation != null) {
					pipeline.annotate(annotation);

					List<CoreMap> sentences = annotation
							.get(CoreAnnotations.SentencesAnnotation.class);
					if (sentences != null) {
						processSentences(sentences, parser.getRulesFile(),parser.getEmail());
					}
				}
			}

		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	private StanfordCoreNLP getNLPPipeline() {
		Properties props = loadNLPProperties();
		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
		pipeline.addAnnotator(new PTBTokenizerAnnotator(true,
				"tokenizeNLs=true,ptb3Escaping=true"));
		return pipeline;
	}

	private Annotation getAnnotation(EMail email) {
		Annotation annotation = null;
		String rowsData = cleanHTML(email);
		if (rowsData != null)
			annotation = new Annotation(rowsData);
		return annotation;
	}

	private Annotation getAnnotation(String rowsData) {
		Annotation annotation = null;
		if (rowsData != null)
			annotation = new Annotation(rowsData);
		return annotation;
	}

	private PrintWriter getPrintWriter() {
		PrintWriter out = null;
		out = new PrintWriter(System.out);
		return out;
	}

	private Properties loadNLPProperties() {

		// get the property value and print it out
		String modelsDirectory = Config.getModelsDirectoryPath();
		// Path to the folder with classifies models
		String regexNERDirecrory = Config.getNERDirectoryPath();
		Properties props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma, ner, regexner");
		String tokenOptions = "tokenize.options";
		// props.setProperty(tokenOptions,
		// "tokenizeNLs=true,normalizeSpace=false,unicodeEllipsis=true,ptb3Escaping=false");
		props.put("regexner.mapping", regexNERDirecrory);
		// SUTime configuration
		String sutimeRules = modelsDirectory + "/sutime/defs.sutime.txt,"
				+ modelsDirectory + "/sutime/english.holidays.sutime.txt,"
				+ modelsDirectory + "/sutime/english.sutime.txt";

		props.setProperty("sutime.rules", sutimeRules);
		props.setProperty("sutime.binders", "0");
		return props;
	}

	private List<CoreLabel> InsertNLsInMatchedSentence(
			List<CoreLabel> matchedTokens, List<CoreLabel> tokensWithNL) {

		int newLineCount = tokensWithNL.size();
		List<Integer> newlineIndexes = new ArrayList<Integer>();
		for (CoreLabel eachToken : tokensWithNL) {

			if (eachToken.value() == "*NL*") {

				newlineIndexes.add(tokensWithNL.indexOf(eachToken));
				//System.//out.println("newline index of NL : "
				//					+ tokensWithNL.indexOf(eachToken));
			}
		}
//		//System.out.println(" macthed token size" + matchedTokens.size());
		for (int eachNLIndex : newlineIndexes) {
			//System.out.println("matchedTokens at index "
//								+ matchedTokens.get(eachNLIndex));
			matchedTokens.add(eachNLIndex, tokensWithNL.get(eachNLIndex));
		}

		return matchedTokens;
	}

	private void processSentences(List<CoreMap> sentences, String rules, EMail email) {
		try{
			//out.println("**********##############################********************");
			//
			CoreMapExpressionExtractor<MatchedExpression> extractor = CoreMapExpressionExtractor
					.createExtractorFromFiles(TokenSequencePattern.getNewEnv(),
							rules);
			Map<String, List<String>> emailData = new LinkedHashMap<String, List<String>>();
			for (CoreMap sentence : sentences) {

				//System.out.println("************************************** ACTUAL SENTENCE BEGIN : \n"
//									+ sentence.toString()+ "  \n  ACTUAL SENTENCE END" );

				List<MatchedExpression> matchedExpressions = extractor.extractExpressions(sentence);

				for (MatchedExpression matched : matchedExpressions) {
					// Print out token information

					CoreMap cm = matched.getAnnotation();
					// Print out matched text and value

					//System.out.println("******* MATCHED SENTENCE BEGIN ***********\n"
//							+ matched.toString() + "\n *******MATCHED SENTENCE ENDD ********");
					Annotation annotation = getAnnotation(matched.toString());
					Properties props = new Properties();
					props.put("annotators",
							"tokenize, ssplit, pos, lemma, ner, regexner");

					StanfordCoreNLP pipeline1 = new StanfordCoreNLP(props);
					pipeline1.addAnnotator(new PTBTokenizerAnnotator(true,
							"tokenizeNLs=true,ptb3Escaping=true"));
					//System.out.println("****ANNOTATED MATCHED ANNORATION BEGIN ******* \n" + annotation + "\n****ANNOTATED MATCHED ANNORATION END *******");
					List<CoreLabel> sentencewithNLtokens = null;
					if (annotation != null) {
						pipeline1.annotate(annotation);

						sentencewithNLtokens = annotation
								.get(CoreAnnotations.TokensAnnotation.class);
					}

					List<CoreLabel> tokensInSentence = cm.get(CoreAnnotations.TokensAnnotation.class);
					tokensInSentence = InsertNLsInMatchedSentence(tokensInSentence, sentencewithNLtokens);


					//System.out.println("ALL TOKENSS WITH NewLine INSERTION ");
					for (CoreLabel l : tokensInSentence) {
						//System.out.println(" " + l);
					}
					//System.out.println("END OF ALL TOKENSS WITH INSERTION ");

					List<List<String>> tableDataRows = new ArrayList<List<String>>();
					List<String> headers = new ArrayList<String>();
					List<String> keys = new ArrayList<String>();
					List<String> values = new ArrayList<String>();

					//System.out.println("*******START RETRIVING THE TOKENS ***********" + " NUM OF TOKENS: " + tokensInSentence.size());

					//System.out.println("matched Annotation: " + matched.getAnnotation()
//					 + "\n ----- Text : " + matched.getText()
//					 + "\n ----- Value:" + matched.getValue().get());
					String s = cm.get(CoreAnnotations.TextAnnotation.class);
					//System.out.println(String.format("+++++++++++++++   Values are: %s \t ", s));

					// SequenceMatchResult result =
					// cm.get(CoreAnnotations.TokensAnnotation.class);

					Integer lastTokenIndex = tokensInSentence.size() -1 ;
					for (CoreLabel token : tokensInSentence) {

						String word = token
								.get(CoreAnnotations.TextAnnotation.class);
						String lemma = token
								.get(CoreAnnotations.LemmaAnnotation.class);
						// String tag =
						// token.getString(CoreAnnotations.PartOfSpeechTagAnnotation.class);
						String pos = token
								.get(CoreAnnotations.PartOfSpeechAnnotation.class);
						String ne = token
								.get(CoreAnnotations.NamedEntityTagAnnotation.class);
						String nne = token
								.get(CoreAnnotations.NormalizedNamedEntityTagAnnotation.class);

						String timexValue = "";                
						if(token.containsKey(TimexAnnotation.class)) 
						{
							Timex tokenTimex =token.get(TimexAnnotation.class);
							timexValue = tokenTimex.value();

						}
						Integer IndexOfToken = tokensInSentence.indexOf(token);

						if (timexValue != "") {
							System.out.println("Matched token: "
								+ token
								+ " word="
										+ word
										+ ", lemma="
										+ lemma
										+ ", pos="
										+ pos
										+ ", ne="
										+ ne
										+ ", nne="
										+ nne
										+", timex="
										+ timexValue
										+ " INDEX : "
										+ IndexOfToken);
						} else {
							System.out.println("Matched token: "
								+ token
								+ " word="
								+ word
								+ ", lemma="
								+ lemma
								+ ", pos="
								+ pos
								+ ", ne="
								+ ne
								+ " INDEX : "
								+ IndexOfToken );
						}


						if (!(word == "*NL*")) {
							if (ne.equals("HOTEL_KEY") && !pos.equals("SYM") &&   (!word.equals(pos))) {
								//System.out.println("1st case word : " + word);
								keys.add(word);
							}
							else if (ne.equals("HOTEL_HEADER") && !pos.equals("SYM") &&   (!word.equals(pos))) {
								//System.out.println("2nd case word : " + word);
								headers.add(word);
							} 
							else if (ne.equals("HOTEL_VALUE") 	&& !pos.equals("SYM")) {
								//System.out.println("3rd case word : " + word);
								if (timexValue == "" && word != null) {
									values.add(word);
								} else if( timexValue != "" ){
									
									//replace T
									if(nne.contains("T")){
										nne = nne.replace("T", " ");
									}
									
									if (!values.contains(nne)) {
										values.add(nne);
									}
								}
								if(IndexOfToken == lastTokenIndex )
								{
									tableDataRows.add(values);
									values = new ArrayList<String>();
								}
							}
						} else if ( ( word == "*NL*") &&  values.size() != 0 ) {

							tableDataRows.add(values);
							values = new ArrayList<String>();

							Iterator<String> valuesIt = values.iterator();
							while (valuesIt.hasNext()) {
								String string = (String) valuesIt.next();
								//System.out.println("value inside : " + string);

							}
						}

					}

					if( keys != null && keys.size()> 0 && tableDataRows != null && tableDataRows.size() >0)
					{
						String keyHeader = "";
						String keyValue = "";
						for (String eachHeader : keys) {
							keyHeader += " " + eachHeader;

						}
						for(List<String> values1:tableDataRows)
						{					
							for (String eachValue : values1) {
								keyValue += " " + eachValue;
							}
							//System.out.println(" MY KEAYYYYYYYYYYYYYYYYY " + keyHeader);
							//System.out.println(" MY VALUEEEEEEEEEEe " + keyValue);
						}


						if(keyHeader.equalsIgnoreCase("From") && keyValue.contains("mailto")){
							//TODO this is for demo purpose to differentiate From email and From place
						}else if(keyHeader.equalsIgnoreCase("To") && keyValue.contains("Subject")){
							//TODO this is for demo purpose to differentiate To email and To place
						}else{

							if(keyHeader.contains("Dear")){
								keyHeader = keyHeader.replace("Dear", "Name");
							}
							if(keyValue.contains("-LRB-"))
								keyValue = keyValue.replace("-LRB-", "(");
							if(keyValue.contains("-RRB-"))
								keyValue = keyValue.replace("-RRB-", ")");

							if(emailData.containsKey(keyHeader)){
								List<String> existingList = emailData.get(keyHeader);
								existingList.add(keyValue);
								emailData.put(keyHeader,existingList);
							}else{
								List<String> list = new ArrayList<String>();
								list.add(keyValue);
								emailData.put(keyHeader, list);
							}
						}
					}
					if( headers != null && headers.size()> 0 && tableDataRows != null && tableDataRows.size() >0)
					{
						String eachHeader = "";
						String eachValue = "";
						for( int i=0; i< (headers.size() ); i++)
						{
							List<String> columnValues = new ArrayList<String>();
							for(int eachRowIndex =0 ; eachRowIndex < tableDataRows.size(); eachRowIndex++)
							{
								columnValues.add(tableDataRows.get(eachRowIndex).get(i));

							}
							emailData.put(headers.get(i),columnValues) ;
						}
					}
					if(emailData != null && emailData.size() > 0)
					{
						for (Map.Entry<String, List<String>> entry : emailData.entrySet())

						{
							String key = entry.getKey();
							List<String> allvalues = entry.getValue();
							//						out.print(" KEYS : " + key + " VALUE: " + allvalues);

						}
					}

				}
				//out.println("**********END********");
			}

			HTMLBuilder htmlBuilder = new HTMLBuilder();
			Car car = new Car();
			car.setData(emailData);
			htmlBuilder.writeToHtML(car, emailNameWithoutExt, email);
			
		}catch(Exception e){
			System.err.print("Exception while processing sentences : " + e.getMessage());
		}
	}


	private String cleanHTML(EMail email) {
		try {
			if (email != null && email.getHtml() != null) {
				String html = email.getHtml();// "<html><head><title>Sample Hello, World Application</title></head><body bgcolor=white><p>To prove that they work, you can execute either of the following links:</body></html>";//
				// //System.//out.println("Html : " + html);
				String htmlString = html.replaceAll("\\<.*?\\>", " ");
				htmlString.replaceAll("&nbsp;", "");
				// //System.//out.println("htmlString : " + htmlString.trim());
				return htmlString;
			}else if(email !=null && email.getBody() != null){
				return email.getBody();
			}
			return null;
		} catch (Exception e) {
			System.out.println("Exception in cleanHTML " + e.getMessage());
			return null;
		}

	}

}
