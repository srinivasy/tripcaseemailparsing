package com.motivity.tripcase.parser;

import java.util.*;

import edu.stanford.nlp.graph.*;
import edu.stanford.nlp.fsm.*;
import edu.stanford.nlp.classify.*;
import edu.stanford.nlp.sequences.*;
import edu.stanford.nlp.wordseg.*;
import edu.stanford.nlp.util.*;

import java.lang.*;

import edu.stanford.nlp.dcoref.*;
import edu.stanford.nlp.process.*;
import edu.stanford.nlp.pipeline.*;
import edu.stanford.nlp.trees.*;
import edu.stanford.nlp.semgraph.*;
import edu.stanford.nlp.ling.*;
import edu.stanford.nlp.tagger.maxent.*;
import edu.stanford.nlp.time.*;
import edu.stanford.nlp.parser.lexparser.*;

import java.io.*;

import edu.stanford.nlp.ie.*;



public class RegexNER
{
	private edu.stanford.nlp.pipeline.StanfordCoreNLP pipeline = null;
	
	CoreNLP coreNLP = new CoreNLP();
	public static void main(String array[]){
		
		RegexNER regexNER = new RegexNER();
		regexNER.RegexAnnotate();
		
	}
	public final String RegexAnnotate()
	{
		String result  = "";
		Properties prop = new Properties();
		InputStream input = null;
		
		String modelsPath = null;
		//String inputText = "D:\\projects\\TripCase_Project\\Stanford_NLP_Tripcase\\source\\TripCase\\mail\\airline\\";

		String inputText = "Booking Reference:   UNRKCP \n Ticketing Airline:   JetBlue\n Ticketnumbers:       279-2107038822";

		try {
		 
			 
		  String propFileName = "config.properties";
		 
		  input = getClass().getClassLoader().getResourceAsStream(propFileName);
		  //input = new FileInputStream(inputStream);
		 
		  // load a properties file
		  prop.load(input);
		 
		  // get the property value and print it out
		  System.out.println(prop.getProperty("database"));
		  
		modelsPath = prop.getProperty("RegexNER");
		//String modelsDirectory = modelsPath + "edu\\stanford\\nlp\\models";
		String modelsDirectory ="D:\\projects\\TripCase_Project\\Stanford_NLP_Tripcase\\source\\TripCase\\models\\stanford-corenlp-3.4-models\\"+ "edu\\stanford\\nlp\\models";
		
		// Path to the folder with classifies models
		//String regexNERDirecrory = modelsPath + "AIRLINE_TRIPCASE.txt";
		String regexNERDirecrory = "D:\\projects\\TripCase_Project\\Stanford_NLP_Tripcase\\source\\TripCase\\models\\stanford-corenlp-3.4-models\\" + "AIRLINE_TRIPCASE.txt";
		
		Properties props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma, ner, regexner");
		props.put("regexner.mapping", regexNERDirecrory);


		// SUTime configuration
		String sutimeRules = modelsDirectory + "\\sutime\\defs.sutime.txt," + modelsDirectory + "\\sutime\\english.holidays.sutime.txt," + modelsDirectory + "\\sutime\\english.sutime.txt";

		props.setProperty("sutime.rules", sutimeRules);
		props.setProperty("sutime.binders", "0");

		CreateModels(props);
	   // pipeline.addAnnotator(new TimeAnnotator("sutime", props));


		// create an empty Annotation just with the given text
		edu.stanford.nlp.pipeline.Annotation annotation = new edu.stanford.nlp.pipeline.Annotation(inputText);

		// run all Annotators on this text    
		pipeline.annotate(annotation);
		String filename = coreNLP.CreateNLPFilesDirectory();
		//String filename = "c:\\temp\\output\\output_" + 1 + ".xml";
		FileOutputStream os = new FileOutputStream(new java.io.File(filename));
		pipeline.xmlPrint(annotation, os);
		os.close();
		InputStream in = new FileInputStream(new File(filename));
	    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
	    StringBuilder out = new StringBuilder();
	    String line;
	    while ((line = reader.readLine()) != null) 
	    {
	            out.append(line);
	    }
		result = out.toString();
	    reader.close();   
		
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		return result;
	}
	public final void CreateModels( Properties props)
	{
		
		pipeline = new StanfordCoreNLP(props);
	
	}
}