package com.motivity.tripcase.parser;

import java.text.SimpleDateFormat;
import java.util.*;
import java.io.*;

import edu.stanford.nlp.pipeline.*;
import edu.stanford.nlp.trees.*;
import edu.stanford.nlp.semgraph.*;
import edu.stanford.nlp.ling.*;
import edu.stanford.nlp.ling.CoreAnnotations.NamedEntityTagAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.graph.*;
import edu.stanford.nlp.fsm.*;
import edu.stanford.nlp.classify.*;
import edu.stanford.nlp.sequences.*;
import edu.stanford.nlp.wordseg.*;
import edu.stanford.nlp.util.*;

import java.lang.*;

import edu.stanford.nlp.dcoref.*;
import edu.stanford.nlp.process.*;


public class CoreNLP
{
	private edu.stanford.nlp.pipeline.StanfordCoreNLP pipeline = null;
	public final String Annotate(String modelsPath, String inputText)
	{
		Properties props = new Properties();
		props.setProperty("annotators", "tokenize, ssplit, pos, lemma, ner, parse, dcoref");
		props.setProperty("outputFormat", "xml");
		props.setProperty("sutime.binders", "0");

		CreateModels( props);

		edu.stanford.nlp.pipeline.Annotation annotation = new edu.stanford.nlp.pipeline.Annotation(inputText);

		// run all Annotators on this text
		pipeline.annotate(annotation);

		String result = "";

		
		try {
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			pipeline.prettyPrint(annotation, new PrintWriter(stream));
			result = stream.toString();
			stream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return result;
	}
	public final void CreateModels(Properties props)
	{
		pipeline = new StanfordCoreNLP(props);

	}
	public  String CreateNLPFilesDirectory()
	{
		Properties prop = new Properties();
		InputStream input = null;
		 
		 try {
			 String propFileName = "config.properties";
			 
			  input = getClass().getClassLoader().getResourceAsStream(propFileName);
			  //input = new FileInputStream(inputStream);
			 
			  // load a properties file
			  prop.load(input);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		  
		 
		// get the property value and print it out
		//System.out.println(prop.getProperty("database"));
		  
		//String modelsPath = prop.getProperty("NLPOutputFileName");
		String file = "D:\\projects\\TripCase_Project\\Stanford_NLP_Tripcase\\source\\TripCase\\mail\\airline\\2019587.eml";
		java.io.File fi = new java.io.File(file);
		if (!fi.getParentFile().exists())
		{
			(new java.io.File(fi.getParent())).mkdir();
		}

		int pos = file.lastIndexOf(".");
		String xmlfilename = file.substring(0, pos);

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyy_HHmmss");
		
		String filename = "c:\\temp\\output\\output_" + sdf.format(cal.getTime()) + ".xml";
		return xmlfilename;
	}

	public final String AnnotateXML(String modelsPath, String inputText)
	{
		String result = "";
		Properties props = new Properties();
		props.setProperty("annotators", "tokenize, ssplit, pos, lemma, ner, parse, dcoref");
		props.setProperty("outputFormat", "xml");
		props.setProperty("sutime.binders", "0");

		CreateModels( props);
		//   pipeline.addAnnotator(new edu.stanford.nlp.pipeline.PTBTokenizerAnnotator(false));
		//   pipeline.addAnnotator(new edu.stanford.nlp.pipeline.WordsToSentencesAnnotator(false));

		// create an empty Annotation just with the given text
		edu.stanford.nlp.pipeline.Annotation annotation = new edu.stanford.nlp.pipeline.Annotation(inputText);

		// run all Annotators on this text    
		pipeline.annotate(annotation);
		String filename = CreateNLPFilesDirectory();
		FileOutputStream os;
		try {
			os = new FileOutputStream(new java.io.File(filename));
			pipeline.xmlPrint(annotation, os);
			os.close();
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
		
	    try {
	    	InputStream in = new FileInputStream(new File(filename));
		    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		    StringBuilder out = new StringBuilder();
		    String line;
		    while ((line = reader.readLine()) != null) 
		    {
		            out.append(line);
		    }
			result = out.toString();
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   
		
		return result;
	}

	public final String TokenDetails(String modelsPath, String inputText)
	{
		Properties props = new Properties();
		props.setProperty("annotators", "tokenize, ssplit, pos, lemma, ner, parse, dcoref");
		props.setProperty("outputFormat", "xml");
		props.setProperty("sutime.binders", "0");

		CreateModels( props);
		//   pipeline.addAnnotator(new edu.stanford.nlp.pipeline.PTBTokenizerAnnotator(false));
		//   pipeline.addAnnotator(new edu.stanford.nlp.pipeline.WordsToSentencesAnnotator(false));

		// create an empty Annotation just with the given text

		edu.stanford.nlp.pipeline.Annotation annotation = new edu.stanford.nlp.pipeline.Annotation(inputText);

		// run all Annotators on this text    
		pipeline.annotate(annotation);


		// these are all the sentences in this document
		//a CoreMap is essentially a Map that uses class objects as keys and has values with custom types
	    List<CoreMap> sentences = annotation.get(SentencesAnnotation.class);
	    
	    String result = "";
	    for(CoreMap sentence: sentences) {
	    	result += "*******************************************SENTENCE BEGIN*****************************************************\n";
			
	        // traversing the words in the current sentence
	        // a CoreLabel is a CoreMap with additional token-specific methods
	        for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
	          String eachtokenDetails = "";
	          // this is the text of the token
	          String word = token.get(TextAnnotation.class);
	          // this is the POS tag of the token
	          String pos = token.get(PartOfSpeechAnnotation.class);
	          // this is the NER label of the token
	          String ne = token.get(NamedEntityTagAnnotation.class);     
	          eachtokenDetails = String.format("Token : %1$s \t POSTag: %2$s\t NERLabel: %3$s \n", word, pos, ne);
				result += eachtokenDetails;
	        }
	        result += "*******************************************SENTENCE END*****************************************************\n";

	        }
	  
		return result;
	}

	public final String TreeDetails(String modelsPath, String inputText)
	{
		Properties props = new Properties();
		props.setProperty("annotators", "tokenize, ssplit, pos, lemma, ner, parse, dcoref");
		props.setProperty("outputFormat", "xml");
		props.setProperty("sutime.binders", "0");

		CreateModels( props);
		//   pipeline.addAnnotator(new edu.stanford.nlp.pipeline.PTBTokenizerAnnotator(false));
		//   pipeline.addAnnotator(new edu.stanford.nlp.pipeline.WordsToSentencesAnnotator(false));

		// create an empty Annotation just with the given text
		edu.stanford.nlp.pipeline.Annotation annotation = new edu.stanford.nlp.pipeline.Annotation(inputText);

		// run all Annotators on this text    
		pipeline.annotate(annotation);
		
		List<CoreMap> sentences = annotation.get(SentencesAnnotation.class);

		String result = "";
		for (edu.stanford.nlp.util.CoreMap sentence : sentences)
		{
			result += "*******************************************SENTENCE TREE BEGIN*****************************************************\n";

			// this is the parse tree of the current sentence
			edu.stanford.nlp.trees.Tree tree = (edu.stanford.nlp.trees.Tree)sentence.get((new edu.stanford.nlp.trees.TreeCoreAnnotations.TreeAnnotation()).getClass());

			
			try {
				//Print the tree as done in Penn Treebank merged files. The formatting should be exactly the same, but we don't print the trailing whitespace found in Penn Treebank trees.
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				tree.pennPrint(new PrintStream(stream));
				result += stream.toString();
				stream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			result += "\n*******************************************SENTENCE TREE END*****************************************************\n";

		}
		return result;
	}

	public final String SemanticGraphDetails(String modelsPath, String inputText)
	{
		Properties props = new Properties();
		props.setProperty("annotators", "tokenize, ssplit, pos, lemma, ner, parse, dcoref");
		props.setProperty("outputFormat", "xml");
		props.setProperty("sutime.binders", "0");

		CreateModels( props);
		//   pipeline.addAnnotator(new edu.stanford.nlp.pipeline.PTBTokenizerAnnotator(false));
		//   pipeline.addAnnotator(new edu.stanford.nlp.pipeline.WordsToSentencesAnnotator(false));

		// create an empty Annotation just with the given text
		edu.stanford.nlp.pipeline.Annotation annotation = new edu.stanford.nlp.pipeline.Annotation(inputText);

		// run all Annotators on this text    
		pipeline.annotate(annotation);

		List<CoreMap> sentences = annotation.get(SentencesAnnotation.class);

		String result = "";
		for (edu.stanford.nlp.util.CoreMap sentence : sentences)
		{
			result += "*****************************SENTENCE SemanticGraph BEGIN*******************************************\n";

			// this is the Stanford dependency graph of the current sentence
			edu.stanford.nlp.semgraph.SemanticGraph dependencies = (edu.stanford.nlp.semgraph.SemanticGraph)sentence.get((new edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation()).getClass());

			result += dependencies.toFormattedString();

			result += "\n*****************************SENTENCE SemanticGraph END********************************************\n";

		}
		return result;
	}

}