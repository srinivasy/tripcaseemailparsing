package com.motivity.tripcase.parser;

import java.io.File;

import com.motivity.tripcase.pojo.Parser;
import com.motivity.tripcase.utils.Config;
import com.motivity.tripcase.utils.ParserBuilder;

public class MainParser {
	public static void main(String[] args) {
		parseAirlineEMails();
	}

	static void parseAirlineEMails(){

		parseAirline();
		parseHotel();
		parseCar();
	}

	private static void parseHotel() {
		File[] files = new File(Config.getEMailPath("hotel")).listFiles();
		//If this pathname does not denote a directory, then listFiles() returns null.
		if(files != null){
			HotelParser airlineParser = new HotelParser();
			ParserBuilder utils = new ParserBuilder();
			for (File file : files) {
				if (file.isFile()) {
					System.out.println("************************* " + file.getName() + " ***********************");
					Parser parser = utils.buildParser(file);
					if(parser != null)
						airlineParser.parse(parser);
				}
			}
		}
	}
	
	private static void parseCar() {
		File[] files = new File(Config.getEMailPath("car")).listFiles();
		//If this pathname does not denote a directory, then listFiles() returns null.
		if(files != null){
			CarParser airlineParser = new CarParser();
			ParserBuilder utils = new ParserBuilder();
			for (File file : files) {
				if (file.isFile()) {
					System.out.println("************************* " + file.getName() + " ***********************");
					Parser parser = utils.buildParser(file);
					if(parser != null)
						airlineParser.parse(parser);
				}
			}
		}
	}

	private static void parseAirline() {
		File[] files = new File(Config.getEMailPath("airline")).listFiles();
		//If this pathname does not denote a directory, then listFiles() returns null.
		if(files != null){
			AirlineParser airlineParser = new AirlineParser();
			ParserBuilder utils = new ParserBuilder();
			for (File file : files) {
				if (file.isFile()) {
					System.out.println("************************* " + file.getName() + " ***********************");
					Parser parser = utils.buildParser(file);
					if(parser != null)
						airlineParser.parse(parser);
				}
			}
		}
	}

}
