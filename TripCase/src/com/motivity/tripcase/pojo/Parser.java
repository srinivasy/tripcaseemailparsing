package com.motivity.tripcase.pojo;

public class Parser {
	
	private EMail email;
	private String emailFile;
	private String rulesFile;
	
	public EMail getEmail() {
		return email;
	}
	public void setEmail(EMail email) {
		this.email = email;
	}
	public String getEmailFile() {
		return emailFile;
	}
	public void setEmailFile(String emailFile) {
		this.emailFile = emailFile;
	}
	public String getRulesFile() {
		return rulesFile;
	}
	public void setRulesFile(String rulesFile) {
		this.rulesFile = rulesFile;
	}
	

}
