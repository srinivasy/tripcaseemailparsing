package com.motivity.tripcase.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.motivity.tripcase.pojo.Airline;
import com.motivity.tripcase.pojo.EMail;
import com.motivity.tripcase.pojo.Flight;


public class XMLBuilder {

	
	public void buildXML(Map<String, List<String>> data, String emailName, EMail email){
		try {
			
			/*DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
	 
			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("Airline");//TODO change this
			doc.appendChild(rootElement);
	 
			
			Element staff = doc.createElement(emailName);
			rootElement.appendChild(staff);
	 
			
			if(data != null){
				for (Map.Entry<String, List<String>> entry : data.entrySet()) {
				    String key = entry.getKey();
				    List<String> values = entry.getValue();
				    
				    for(String eachValue: values)
				    {
					    Element firstname = doc.createElement(key);
						firstname.appendChild(doc.createTextNode(eachValue));
						staff.appendChild(firstname);
				    }
				}
				
				rootElement.appendChild(staff);
				// write the content into xml file
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(doc);
				System.out.println("path " + Config.getOutputFileXMLName(emailName));
				StreamResult result = new StreamResult(new File(Config.getOutputFileXMLName(emailName)));
		 
				// Output to console for testing
				// StreamResult result = new StreamResult(System.out);
		 
				transformer.transform(source, result);*/
			if(data != null){
				Airline airline = buildAirlineObjects(data, emailName);
			    if(airline != null){
			     new HTMLBuilder().writeToHtML(airline, emailName, email);
			    }
				System.out.println("File saved!");
			}

		} catch (Exception tfe) {
			tfe.printStackTrace();
		}
	}
	private Airline buildAirlineObjects(Map<String, List<String>> data, String emailName){
		  if(data != null){

		   Airline airline = new Airline();
		   airline.setData(data);

		   Iterator<Map.Entry<String, List<String>>> entries = data.entrySet().iterator();
		   if(entries.hasNext()){
		    List<Flight> flights = new ArrayList<Flight>();
		    while (entries.hasNext()) {
		     Map.Entry<String, List<String>> entry = entries.next();
		     String key = entry.getKey();
		     //     System.out.println("Key = " + key + ", Value = " + entry.getValue());
		     List<String> values = entry.getValue();
		     if(values != null && values.size() > 1){
		      if(flights.size() == 0){
		       for (int i = 0; i < values.size(); i++) {
		        Flight flight = new Flight();
		        flights.add(flight);
		       }
		      }
		      for (int i = 0; i < values.size(); i++) {
		       String string = (String) values.get(i);
		       //       System.out.println("values inside : " + string);
		       //       System.out.println("size of flights : " + flights.size());
		       key = key.trim();
		       if(key.equalsIgnoreCase("Flightno") || key.equalsIgnoreCase("Flight")){
		        flights.get(i).setFlightno(string);
		       }else if(key.equalsIgnoreCase("Date")){
		        flights.get(i).setDate(string);
		       }else if(key.equalsIgnoreCase("Source")){
		        flights.get(i).setFrom(string);
		       }else if(key.equalsIgnoreCase("Dept")){
		        flights.get(i).setDept(string);
		       }else if(key.equalsIgnoreCase("Dest")){
		        flights.get(i).setTo(string);
		       }else if(key.equalsIgnoreCase("Arrv")){
		        flights.get(i).setArrv(string);
		       }else if(key.equalsIgnoreCase("Status")){
		        flights.get(i).setStatus(string);
		       }else if(key.equalsIgnoreCase("Class")){
		        flights.get(i).setClas(string);
		       }else if(key.equalsIgnoreCase("Meal")){
		        flights.get(i).setMeal(string);
		       }
		       
		      }
		     }
		    }

		    //    System.out.println("FLights size at the end : " + flights.size());
		    airline.setFlights(flights);
		   }
		   return airline;
		  }
		  return null;
		 }

}
