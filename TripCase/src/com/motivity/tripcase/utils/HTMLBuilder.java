package com.motivity.tripcase.utils;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.motivity.tripcase.parser.AirlineParser;
import com.motivity.tripcase.pojo.Airline;
import com.motivity.tripcase.pojo.Car;
import com.motivity.tripcase.pojo.EMail;
import com.motivity.tripcase.pojo.Flight;
import com.motivity.tripcase.pojo.Hotel;

public class HTMLBuilder {


	private String createTableHead(String value){
		return String.format("%s%s%s", "<th bgcolor=#ADD8E6>",value,"</th>");
	}
	public void writeToHtML(Airline airline, String emailName, EMail email) {
		try{
			Map<String, List<String>> data = airline.getData();
			Iterator<Map.Entry<String, List<String>>> entries = data.entrySet().iterator();
			if(entries.hasNext()){

				PrintWriter msgstr = null;
				String fileName = AirlineParser.emailNameWithoutExt;
				msgstr = new PrintWriter(new FileWriter( Config.getOutputFileHTMLName(fileName, "airline")));
				String leftRow = "<td align= left width=2% bgcolor=#ADD8E6>";
				String rightRow = "<td align=left width=8% bgcolor=#87CEFA>";
				String startRow = "<tr>";
				String endRow = "</tr>";
				String endDesc = "</td>";

				msgstr.println();
				msgstr.println("<title>" +  emailName  + "</title>");
				
				msgstr.println("<h4 align=\"center\" > Airline Information  </h4>");
				
				printEmailInfo(msgstr, email);
				
				msgstr.println("<h4>  Booking Info : </h4>");
				msgstr.println("<table border=1 cellspacing=2 cellpadding=2 >");
				while (entries.hasNext()) {
					Map.Entry<String, List<String>> entry = entries.next();
					String key = entry.getKey();
					List<String> values = entry.getValue();
					if(values.size() == 1){
						//						System.out.println("Key = " + key + ", Value = " + entry.getValue());
						msgstr.println(startRow);
						msgstr.println(leftRow);
						msgstr.println(key );
						msgstr.println(endDesc);
						msgstr.println(rightRow);
						msgstr.println( values.get(0));
						msgstr.println(endDesc);
						msgstr.println(endRow);
					}
				}

				msgstr.println("</table >");
				msgstr.println("<br>");

				List<Flight> flights = airline.getFlights();
				String desc = "<td align=center bgcolor=#87CEFA>";
				if(flights != null ){
					Iterator<Flight> flightIterator = flights.iterator();
					if(flightIterator.hasNext()){
						
						msgstr.println("<h4> Flight Info : </h4>");
						msgstr.println("<table border=1 cellspacing=2 cellpadding=2 width=100% >");
						boolean headingCreated = false;
						while (flightIterator.hasNext()) {
							Flight flight = (Flight) flightIterator.next();
							if(!headingCreated){

								if(flight.getFlightno() != null){
									msgstr.println(createTableHead("Flight Number"));
								}
								if(flight.getDate() != null){
									msgstr.println(createTableHead("Date"));
								}
								if(flight.getFrom() != null){
									msgstr.println(createTableHead("From"));
								}
								if(flight.getDept() != null){
									msgstr.println(createTableHead("Dept"));
								}
								if(flight.getTo() != null){
									msgstr.println(createTableHead("To"));
								}
								if(flight.getArrv() != null){
									msgstr.println(createTableHead("Arrival"));
								}
								if(flight.getStatus() != null){
									msgstr.println(createTableHead("Status"));
								}
								if(flight.getClas() != null){
									msgstr.println(createTableHead("Class"));
								}
								if(flight.getMeal() != null){
									msgstr.println(createTableHead("Meal"));
								}
								headingCreated = true;
							}


							msgstr.println(startRow);
							if(flight.getFlightno() != null){
								msgstr.println(desc);
								msgstr.println((flight.getFlightno()));
								msgstr.println(endDesc);
							}

							String date = flight.getDate();
							if(date != null){
								msgstr.println(desc);
								msgstr.println(date);
								msgstr.println(endDesc);
							}

							if(flight.getFrom() != null){
								msgstr.println(desc);
								msgstr.println((flight.getFrom()));
								msgstr.println(endDesc);
							}


							if(flight.getDept() != null){
								msgstr.println(desc);
								msgstr.println((flight.getDept()));
								msgstr.println(endDesc);
							}

							if(flight.getTo() != null){
								msgstr.println(desc);
								msgstr.println((flight.getTo()));
								msgstr.println(endDesc);
							}

							if(flight.getArrv() != null){
								msgstr.println(desc);
								msgstr.println((flight.getArrv()));
								msgstr.println(endDesc);
							}

							if(flight.getStatus() != null){
								msgstr.println(desc);
								msgstr.println((flight.getStatus()));
								msgstr.println(endDesc);
							}

							if(flight.getClas() != null){
								msgstr.println(desc);
								msgstr.println((flight.getClas()));
								msgstr.println(endDesc);
							}
							
							if(flight.getMeal() != null){
								msgstr.println(desc);
								msgstr.println((flight.getMeal()));
								msgstr.println(endDesc);
							}
							msgstr.println(endRow);


						}

						msgstr.println("</table >");
						msgstr.println("<br>");
					}

				}

				msgstr.close();
			}

			System.out.println("Output generated successfully");
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}
	
	public void writeToHtML(Hotel hotel, String emailName, EMail email) {
		try{
			Map<String, List<String>> data = hotel.getData();
			Iterator<Map.Entry<String, List<String>>> entries = data.entrySet().iterator();
			if(entries.hasNext()){

				PrintWriter msgstr = null;
				String fileName = AirlineParser.emailNameWithoutExt;
				
				msgstr = new PrintWriter(new FileWriter( Config.getOutputFileHTMLName(emailName, "hotel")));
				String leftRow = "<td align= left width=4% bgcolor=#ADD8E6>";
				String rightRow = "<td align=left width=6% bgcolor=#87CEFA>";
				String startRow = "<tr>";
				String endRow = "</tr>";
				String endDesc = "</td>";

				msgstr.println();
				msgstr.println("<title>" +  emailName  + "</title>");
				
				msgstr.println("<h4 align=\"center\" > Hotel Information  </h4>");
				
				printEmailInfo(msgstr, email);
				
				msgstr.println("<h4>  Booking Info : </h4>");
				msgstr.println("<table border=1 cellspacing=2 cellpadding=2 >");
				while (entries.hasNext()) {
					Map.Entry<String, List<String>> entry = entries.next();
					String key = entry.getKey();
					List<String> values = entry.getValue();
					if(values.size() == 1){
						//						System.out.println("Key = " + key + ", Value = " + entry.getValue());
						msgstr.println(startRow);
						msgstr.println(leftRow);
						msgstr.println(key );
						msgstr.println(endDesc);
						msgstr.println(rightRow);
						msgstr.println( values.get(0));
						msgstr.println(endDesc);
						msgstr.println(endRow);
					}
				}

				msgstr.println("</table >");
				msgstr.println("<br>");


				msgstr.close();
			}

			System.out.println("Output generated successfully");
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}
	
	public void writeToHtML(Car car, String emailName, EMail email) {
		try{
			Map<String, List<String>> data = car.getData();
			Iterator<Map.Entry<String, List<String>>> entries = data.entrySet().iterator();
			if(entries.hasNext()){

				PrintWriter msgstr = null;
				String fileName = AirlineParser.emailNameWithoutExt;
				
				
				msgstr = new PrintWriter(new FileWriter( Config.getOutputFileHTMLName(emailName, "car")));
				String leftRow = "<td align= left width=4% bgcolor=#ADD8E6>";
				String rightRow = "<td align=left width=6% bgcolor=#87CEFA>";
				String startRow = "<tr>";
				String endRow = "</tr>";
				String endDesc = "</td>";

				msgstr.println();
				msgstr.println("<title>" +  emailName  + "</title>");
				
				msgstr.println("<h4 align=\"center\" > Car Rental Information  </h4>");
				
				printEmailInfo(msgstr, email);
				
				msgstr.println("<h4>  Booking Info : </h4>");
				msgstr.println("<table border=1 cellspacing=2 cellpadding=2 >");
				while (entries.hasNext()) {
					Map.Entry<String, List<String>> entry = entries.next();
					String key = entry.getKey();
					List<String> values = entry.getValue();
					if(values.size() == 1){
						//						System.out.println("Key = " + key + ", Value = " + entry.getValue());
						msgstr.println(startRow);
						msgstr.println(leftRow);
						msgstr.println(key );
						msgstr.println(endDesc);
						msgstr.println(rightRow);
						msgstr.println( values.get(0));
						msgstr.println(endDesc);
						msgstr.println(endRow);
					}
				}

				msgstr.println("</table >");
				msgstr.println("<br>");


				msgstr.close();
			}

			System.out.println("Output generated successfully");
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}
	
	private void printEmailInfo(PrintWriter msgstr, EMail email) {
		
		String leftRow = "<td align= left width=2% bgcolor=#ADD8E6>";
		String rightRow = "<td align=left width=8% bgcolor=#87CEFA>";
		String startRow = "<tr>";
		String endRow = "</tr>";
		String endDesc = "</td>";
				
		msgstr.println("<h4> Email Info : </h4>");
		msgstr.println("<table border=1 cellspacing=2 cellpadding=2 >");

		msgstr.println("<tr>");
		msgstr.println(leftRow);
		msgstr.println("From " );
		msgstr.println(endDesc);
		msgstr.println(rightRow);
		msgstr.println( ((email.getFromAddress() ==null) ? "" : email.getFromAddress() ) + "<br>");
		msgstr.println(endDesc);
		msgstr.println("</tr>");

		msgstr.println("<tr>");
		msgstr.println(leftRow);
		msgstr.println("To ");
		msgstr.println(endDesc);
		msgstr.println(rightRow);
		msgstr.println( ((email.getToAddress() ==null) ? "" : email.getToAddress() ) + "<br>");
		msgstr.println(endDesc);
		msgstr.println("</tr>");

		/*	msgstr.println("<tr>");
		msgstr.println(leftRow);
		msgstr.println("Date ");
		msgstr.println(endDesc);
		msgstr.println(rightRow);
		msgstr.println(((email.getReceivedDate() == null) ? "" : email.getReceivedDate()) + "<br>");
		msgstr.println(endDesc);
		msgstr.println("</tr>");*/

		msgstr.println("<tr>");
		msgstr.println(leftRow);
		msgstr.println("Subject ");
		msgstr.println(endDesc);
		msgstr.println(rightRow);
		msgstr.println(((email.getSubject() == null) ? "No subject" : email.getSubject()) + "<br>");
		msgstr.println(endDesc);
		msgstr.println("</tr>");



		msgstr.println("</table >");
		msgstr.println("<br>");
	}
}
