# Uses TokensRegex library to split further split tokens with -

# Map variable names to annotation keys
//headerTag = { type: "CLASS", value: "edu.stanford.nlp.ling.CoreAnnotations$PartOfSpeechTagAnnotation" }
ner = { type: "CLASS", value: "edu.stanford.nlp.ling.CoreAnnotations$NamedEntityTagAnnotation" }
tokens = { type: "CLASS", value: "edu.stanford.nlp.ling.CoreAnnotations$TokensAnnotation" }

# Indicates matched expressions should replace the tokens annotation
options.matchedExpressionsAnnotationKey = tokens;

# Indicates that the matched expressions and tokens should be combined
options.extractWithTokens = TRUE;

# Indicates that the matched expressions should be flattened to be just tokens
options.flatten = TRUE;

# Mark units  

# Define ruleType to be over tokens
ENV.defaults["ruleType"] = "tokens"
# Case insensitive pattern matching (see java.util.regex.Pattern flags)
ENV.defaultStringPatternFlags = 2
# Indicates that after matching, and computing the result, the result should be placed in the tokens annotation
ENV.defaultResultAnnotationKey = tokens


# Define rule where upon matching tokens with -, the matched token is split

#$NamePattern = ((["Dear"])([]{1,3}));

#$ConfirmationIDPattern = ((["Confirmation"]["ID"][":"]) ([]{1,1}));
$ConfirmationIDPattern = ((["Your"]["confirmation"]["number"]["is"][":"]) ([]{1,1}));
$ArrivalDatePattern = ((["Arrival"]["Date"][":"]) ([]{1,4}));


ENV.defaultResultAnnotationKey = tokens
# Define rule where upon matching tokens with -, the matched token is split
ENV.defaults["priority"] = 1
ENV.defaults["ruleType"] = "tokens"
ENV.defaults["stage"] = 1
{
  pattern: ($ConfirmationIDPattern), 
  action: ( Annotate($1, ner, "HOTEL_KEY"), Annotate($2, ner , "HOTEL_VALUE")) 
}
{
  pattern: ($ArrivalDatePattern), 
  action: ( Annotate($1, ner, "HOTEL_KEY"), Annotate($2, ner , "HOTEL_VALUE")) 
}


ENV.defaults["ruleType"] = "tokens"
ENV.defaults["stage"] = 2
//ENV.defaults["priority"] = 2
{
  ruleType: "tokens",
  pattern: ( $ConfirmationIDPattern ), 
  result:  $$0.nodes
}

{
  ruleType: "tokens",
  pattern: ( $ArrivalDatePattern ), 
  result:  $$0.nodes
}


